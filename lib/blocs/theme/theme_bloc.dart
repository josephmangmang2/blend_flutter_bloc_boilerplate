import 'package:blend_flutter_bloc_boilerplate/blocs/theme/theme.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
// kevin
class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  void onLightThemeChange() => add(LightTheme());

  void onDarkThemeChange() => add(DarkTheme());

  void onDecideThemeChange() => add(DecideTheme());

  @override
  ThemeState get initialState => ThemeState.lightTheme();

  @override
  Stream<ThemeState> mapEventToState(ThemeEvent event) async* {
    if (event is DecideTheme) {
      print('inside Theme decision body');
      final int optionValue = await getOption();
      if (optionValue == 0) {
        yield ThemeState.lightTheme();
      } else if (optionValue == 1) {
        yield ThemeState.darkTheme();
      }
    }

    if (event is DarkTheme) {
      print('inside darktheme body');

      yield ThemeState.darkTheme();
      try {
        _saveOptionValue(1);
      } catch (_) {
        throw Exception('Could not persist change');
      }
    }
    if (event is LightTheme) {
      print('inside LightTheme body');

      yield ThemeState.lightTheme();
      try {
        _saveOptionValue(0);
      } catch (_) {
        throw Exception('Could not persist change');
      }
    }
  }
}

Future<void> _saveOptionValue(int optionValue) async {
  final SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setInt('theme_option', optionValue);
  print('Saving option value as $optionValue successfully');
}

Future<int> getOption() async {
  final SharedPreferences preferences = await SharedPreferences.getInstance();
  final int option = preferences.get('theme_option') ?? 0;
  return option;
}

//Use it where needed, just call it out where need
final ThemeBloc changeThemeBloc = ThemeBloc()..onDecideThemeChange();
