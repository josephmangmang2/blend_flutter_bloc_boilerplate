import 'package:blend_flutter_bloc_boilerplate/data/provider/data_provider.dart';
import 'package:blend_flutter_bloc_boilerplate/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserProvider extends DataProvider<User> {
  // TODO configure firebase for both ios and android
  final _userCollections = Firestore.instance.collection('users');

  @override
  Future<void> add(User model) {
    return _userCollections.add(model.toMap());
  }

  @override
  Future<void> delete(User model) {
    return _userCollections.document(model.id).delete();
  }

  @override
  Stream<User> get(String id) {
    return _userCollections
        .document(id)
        .snapshots()
        .map((doc) => User.fromMap(doc.data));
  }

  @override
  Stream<List<User>> getAll() {
    return _userCollections.snapshots().map(
        (snapshot) => snapshot.documents.map((doc) => User.fromMap(doc.data)));
  }

  @override
  Stream<List<User>> search(String query) {
    return _userCollections.where('name', isEqualTo: query).snapshots().map(
        (snapshot) => snapshot.documents.map((doc) => User.fromMap(doc.data)));
  }

  @override
  Future<void> update(User model) {
    return _userCollections.document(model.id).setData(model.toMap());
  }
}
