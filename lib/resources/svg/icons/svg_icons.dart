/// Class holder for svg icon path
/// Use this class to hold your svg icon name and put your svg files to
///
/// Svg icon location: lib/resources/svg/icons
///
/// To use this   SvgPicture.asset(SvgIcons.about)

class SvgIcons {
  SvgIcons._();

  static const _rootPath = 'lib/resources/svg/icons';
  static const about = _rootPath + '/about.svg';
}
