
import 'package:flutter/material.dart';

///
/// Holder class for reusable colors
///
class ColorRes {
  ColorRes._();

  static const text_primary = Colors.black87;
  static const text_secondary = Color(0xFFB0B0B0);


  static const toggleableActiveColor = Color(0xFF64DBB8);
}
