import 'package:blend_flutter_bloc_boilerplate/blocs/theme/theme.dart';
import 'package:blend_flutter_bloc_boilerplate/data/repository/user_repository.dart';
import 'package:blend_flutter_bloc_boilerplate/lang/app_localizations.dart';
import 'package:blend_flutter_bloc_boilerplate/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  // Initialize any logic before app launch

  final ThemeBloc themeBloc = changeThemeBloc;
  runApp(MultiRepositoryProvider(
    // Initiate any repository that you want to be accessible to entire app/widget tree here
    // this will only create 1 instance of its kind
    providers: [
      RepositoryProvider<UserRepository>(
        // create 1 instance of UserRepository access via RepositoryProvider.of<UserRepository>(context);
        create: (BuildContext context) => UserRepository(),
      )
    ],
    child: MultiBlocProvider(
      // Initiate any bloc that you want to be accessible to entire app/widget tree here
      providers: [
        BlocProvider<ThemeBloc>(
          create: (BuildContext context) => themeBloc,
        ),
      ],
      child: BlocBuilder(
        bloc: themeBloc,
        builder: (BuildContext context, ThemeState state) {
          return MaterialApp(
            theme: state.themeData,
            // List all of the app's supported locales here
            supportedLocales: const <Locale>[
              Locale('en', 'US'),
              Locale('it', 'IT'),
              Locale('fil', 'PH'),
            ],
            // These delegates make sure that the localization data for the proper language is loaded
            localizationsDelegates: [
              // A class which loads the translations from JSON files
              AppLocalizations.delegate,
              // Built-in localization of basic text for Material widgets
              GlobalMaterialLocalizations.delegate,
              // Built-in localization for text direction LTR/RTL
              GlobalWidgetsLocalizations.delegate,
            ],
            // Returns a locale which will be used by the app
            localeResolutionCallback: (locale, supportedLocales) {
              // Check if the current device locale is supported
              for (var supportedLocale in supportedLocales) {
                if (supportedLocale.languageCode == locale.languageCode &&
                    supportedLocale.countryCode == locale.countryCode) {
                  return supportedLocale;
                }
              }
              // If the locale of the device is not supported, use the first one
              // from the list (English, in this case).
              return supportedLocales.first;
            },
            home: SplashScreen(),
          );
        },
      ),
    ),
  ));
}
