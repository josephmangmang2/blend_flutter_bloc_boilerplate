import 'package:equatable/equatable.dart';

///
/// Sample model class for user
/// that extend Equatable class to facilitate [operator==] and [hashCode] overrides.
///
class User extends Equatable {
  final String id;
  final String name;

  User({this.id, this.name});

  @override
  List<Object> get props => [id, name];

  Map<String, dynamic> toMap() => {'id': id, 'name': name};

  factory User.fromMap(Map<String, dynamic> map) =>
      User(id: map['id'], name: map['name']);
}
