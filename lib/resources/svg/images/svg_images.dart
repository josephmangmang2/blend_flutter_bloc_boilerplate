/// Class holder for svg image path
/// Use this class to hold your svg image name and put your svg files to
///
/// Svg icon location: lib/resources/svg/images
///
/// To use this   SvgPicture.asset(SvgImages.sample)
///
class SvgImages {
  SvgImages._();

  static const _rootPath = 'lib/resources/svg/images';

  static const sample = _rootPath + '/sample.svg';
}
