import 'package:blend_flutter_bloc_boilerplate/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class UsersEvent extends Equatable {
  const UsersEvent();
}

class StartFetchUsers extends UsersEvent {
  @override
  List<Object> get props => [];
}

class ProcessFirebaseResponse extends UsersEvent {
  final List<User> users;

  ProcessFirebaseResponse(this.users);

  @override
  List<Object> get props => [users];
}
