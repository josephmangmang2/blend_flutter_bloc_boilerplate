
///
/// Holder class for reusable number/size
///

class DimenRes {
  DimenRes._();

  static const display3 = 35.0;
  static const display2 = 28.0;
  static const display1 = 24.0;
  static const headline = 20.0;
  static const title = 16.0;
  static const subhead = 18.0;
  static const body1 = 15.0;
  static const caption = 15.0;
  static const menu = 16.0;
  static const button = 16.0;

  static const body_text_height = 1.5;

  static const icon = 15.0;
  static const toolbar_title = 32.0;
}
