import 'package:blend_flutter_bloc_boilerplate/data/provider/data_provider.dart';

///
/// Class that handle data manipulation
/// extend this class and implement any logical condition when getting data
///
abstract class Repository<Model, DP extends DataProvider> {
  final DP dataProvider;

  Repository(this.dataProvider);

  Stream<List<Model>> search(String query) => dataProvider.search(query);

  Future<void> update(Model model) => dataProvider.update(model);

  Future<void> add(Model model) => dataProvider.add(model);

  Future<void> delete(Model model) => dataProvider.delete(model);

  Stream<List<Model>> getAll() => dataProvider.getAll();

  Stream<Model> get(String id) => get(id);
}
