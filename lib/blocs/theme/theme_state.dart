import 'package:blend_flutter_bloc_boilerplate/resources/resources.dart';
import 'package:flutter/material.dart';
// kevin
class ThemeState {
  final ThemeData themeData;

  ThemeState({@required this.themeData});

  factory ThemeState.lightTheme() => ThemeState(
      themeData: ThemeData(
          // TODO change font family with your own font
          fontFamily: 'ModernEra',
          brightness: Brightness.light,
          buttonTheme: ButtonThemeData(
              textTheme: ButtonTextTheme.primary, buttonColor: Colors.white),
          canvasColor: Colors.white,
          backgroundColor: Colors.white,
          buttonColor: Colors.white,
          accentColor: Colors.blue,
          toggleableActiveColor: ColorRes.toggleableActiveColor));

  factory ThemeState.darkTheme() => ThemeState(
      themeData: ThemeData(
          // TODO change font family with your own font
          fontFamily: 'ModernEra',
          buttonTheme: ButtonThemeData(buttonColor: Colors.black),
          brightness: Brightness.dark,
          canvasColor: Colors.black,
          backgroundColor: Colors.black,
          buttonColor: Colors.black,
          accentColor: Colors.blue,
          toggleableActiveColor: ColorRes.toggleableActiveColor));
}
