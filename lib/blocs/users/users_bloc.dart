import 'dart:async';
import 'package:blend_flutter_bloc_boilerplate/data/repository/user_repository.dart';
import 'package:blend_flutter_bloc_boilerplate/models/user.dart';
import 'package:bloc/bloc.dart';
import './users.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  final UserRepository userRepository;

  StreamSubscription<List<User>> _usersStreamSubscription;

  UsersBloc(this.userRepository);

  @override
  UsersState get initialState => InitialUsersState();

  @override
  Stream<UsersState> mapEventToState(
    UsersEvent event,
  ) async* {
    if (event is StartFetchUsers) {
      yield* _mapStartFetchUsers(event);
    }
    if (event is ProcessFirebaseResponse) {
      yield UsersLoaded(event.users);
    }
  }

  @override
  Future<Function> close() {
    // close opened subscription
    _usersStreamSubscription?.cancel();
    return super.close();
  }

  Stream<UsersState> _mapStartFetchUsers(StartFetchUsers event) async* {
    // listen for any users changes and fire an event
    _usersStreamSubscription = userRepository
        .getAll() //since we cannot yield directly here we need to pass this response via event
        .listen((users) => add(ProcessFirebaseResponse(users)));
  }
}
