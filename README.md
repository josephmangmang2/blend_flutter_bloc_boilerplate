# blend_flutter_bloc_boilerplate

Flutter application boilerplate, with flutter_bloc & firestore included

## Introduction 

Creating a project without a guideline is confusing when working with a team. Guidelines and proper project structures help other members follow the same path and will allow new team members to adapt easily to the new project environment.
 
This repository is made to make sure that everyone who is working with us is on the same page. 
Please take your time reading and have a look at the linked resources. If there are any questions, please ask.

Some codes, structures, and ideas here are from the previous project. The main goal here is to improve productivity and promote faster adaptability for the new dev to a new project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

### Flutter Boilerplate

Libraries and tools included:

  * [equatable](https://pub.dev/packages/equatable)
  * [flutter_cupertino_localizations](https://pub.dev/packages/flutter_cupertino_localizations)
  * flutter_localizations
  * [shared_preferences](https://pub.dev/packages/shared_preferences)
  * [flutter_svg](https://pub.dev/packages/flutter_svg)
  * [flutter_bloc](https://pub.dev/packages/flutter_bloc)
  * [bloc](https://pub.dev/packages/bloc)
  * [cloud_firestore](https://pub.dev/packages/cloud_firestore)
  
## Coding Style Guides
 ***We care about our code style.***
 
We try to stick as much as we can to the official Dart guidelines
- [https://dart.dev/guides/language/effective-dart](https://dart.dev/guides/language/effective-dart)
- [https://github.com/flutter/flutter/wiki/Style-guide-for-Flutter-repo](https://github.com/flutter/flutter/wiki/Style-guide-for-Flutter-repo)

### Architecture
We try to use Flutter official architecture (BLoC) and use flutter_bloc library.

A BLoC stands as a middleman between a source of data in your app (e.g an API response) and widgets that need the data. It receives streams of events/data from the source, handles any required business logic and publishes streams of data changes to widgets that are interested in them.

![picture](readme_img/bloc_architecture_diagram.png)


You can read more about BLoC here: - [https://medium.com/flutterpub/architecting-your-flutter-project-bd04e144a8f1]

You can read more about flutter_bloc here: - [https://bloclibrary.dev/#/whybloc]

## Project Structure
Separate code in independent level

![picture](readme_img/project_structure_1.png) ![picture](readme_img/project_structure_2.png) 

I added class doc with explanation and comment inside class/file

### Performance
Here will be listed somethings, what you should avoid to increase performance
* Avoid creating methods for widgets. If you would like to do so, create a Stateless widget class for it, or use [https://medium.com/@remirousselet/flutter-reducing-widgets-boilerplate-3e635f10685e]

### Conclusion
We will be happy to answer any questions that you may have on this approach, and if you want to lend a hand with the boilerplate then please feel free to submit an issue and/or pull request 🙂

### Thanks!