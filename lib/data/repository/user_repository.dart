import 'package:blend_flutter_bloc_boilerplate/data/provider/user_provider.dart';
import 'package:blend_flutter_bloc_boilerplate/data/repository/repository.dart';
import 'package:blend_flutter_bloc_boilerplate/models/user.dart';

class UserRepository extends Repository<User, UserProvider> {
  UserRepository() : super(UserProvider());
}
