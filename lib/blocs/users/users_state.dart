import 'package:blend_flutter_bloc_boilerplate/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class UsersState extends Equatable {
  const UsersState();
}

class InitialUsersState extends UsersState {
  @override
  List<Object> get props => [];
}

class UsersLoaded extends UsersState {
  final List<User> users;

  UsersLoaded(this.users);

  @override
  List<Object> get props => [users];
}
