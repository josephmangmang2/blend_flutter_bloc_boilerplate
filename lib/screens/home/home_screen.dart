import 'package:blend_flutter_bloc_boilerplate/lang/app_localizations.dart';
import 'package:blend_flutter_bloc_boilerplate/resources/values/values.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Center(
        child: Text(
          AppLocalizations.of(context).translate('welcome_home'),
          style: TextStyle(fontSize: 20, color: ColorRes.text_primary),
        ),
      ),
    ));
  }
}
