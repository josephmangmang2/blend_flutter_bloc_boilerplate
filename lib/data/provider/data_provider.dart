///
/// Abstract class for common data actions
///
/// This class expect to be use with firebase firestore
///

abstract class DataProvider<Model> {

  Stream<List<Model>> search(String query);

  Future<void> update(Model model);

  Future<void> add(Model model);

  Future<void> delete(Model model);

  Stream<List<Model>> getAll();

  Stream<Model> get(String id);
}
